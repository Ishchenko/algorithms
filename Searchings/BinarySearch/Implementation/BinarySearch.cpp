#include "BinarySearch.h"

int BinarySearch(int* A, int value, int left, int right)
{
	if (left > right) return -1;
	int middle = (left + right) / 2;
	if (A[middle] > value) return BinarySearch(A, value, left, middle - 1);
	if (A[middle] < value) return BinarySearch(A, value, middle + 1, right);
	return middle;
}
