#include "QuickSort.h"
#include <cstdlib>

void Swap(int &first, int &second)
{
	if (first == second) return;

	first ^= second;
	second ^= first;
	first ^= second;
}

// 'Default' QuickSort
void QuickSort(int* A, int start, int end)
{
	if (start >= end) return;

	int pivotIndex = Partition(A, start, end);
	QuickSort(A, start, pivotIndex - 1);
	QuickSort(A, pivotIndex + 1, end);
}

int Partition(int* A, int start, int end)
{
	int left = start + 1, right = end;
	
	// In this implementation, we take leftmost element as a pivot
	int pivot = A[start];
	
	while (true)
	{
		while (A[left] < pivot) if (++left == end) break;
		while (A[right] > pivot) --right;

		if (left < right) Swap(A[left], A[right]);
		else break;
	}
	
	Swap(A[right], A[start]);
	return right;
}


// Improved version - 3-way QuickSort
// Useful for arrays with duplicate values
void QuickSort3(int* A, int start, int end)
{
	if (start >= end) return;

	int left = start, right = end, i = start + 1, pivot = A[start];
	
	while (i <= right)
	{
		if (A[i] < pivot)
		{
			Swap(A[left], A[i]);
			++left;
			++i;
		}
		else if (A[i] > pivot)
		{
			while (A[right] > pivot) --right;
			Swap(A[right], A[i]);
			--right;
		}
		else					
			++i;
	}

	QuickSort3(A, start, left - 1);
	QuickSort3(A, right + 1, end);
}
